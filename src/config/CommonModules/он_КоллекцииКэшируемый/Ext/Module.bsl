﻿//
// Кэшируемые коллекции
// 

// Возвращает коллекцию веб цветов
//
// Параметры:  
//
// Возвращаемое значение: 
// 	Структура
//
Функция ЦветаВебЗначения() Экспорт
	
	Результат	= Новый Структура;
	Результат.Вставить("Аквамарин", "7fffd4");
	Результат.Вставить("АкварельноСиний", "f0f8ff");
	Результат.Вставить("АнтикБелый", "faebd7");
	Результат.Вставить("Бежевый", "f5f5dc");
	Результат.Вставить("Белоснежный", "fffafa");
	Результат.Вставить("Белый", "ffffff");
	Результат.Вставить("Бирюзовый", "40e0d0");
	Результат.Вставить("БледноБирюзовый", "afeeee");
	Результат.Вставить("БледноЗеленый", "98fb98");
	Результат.Вставить("БледноЗолотистый", "eee8aa");
	Результат.Вставить("БледноКрасноФиолетовый", "db7093");
	Результат.Вставить("БледноЛиловый", "e6e6fa");
	Результат.Вставить("БледноМиндальный", "ffebcd");
	Результат.Вставить("БледноСиреневый", "d8bfd8");
	Результат.Вставить("Васильковый", "6495ed");
	Результат.Вставить("ВесеннеЗеленый", "00ff7f");
	Результат.Вставить("Голубой", "a6caf0");
	Результат.Вставить("ГолубойСКраснымОттенком", "fff0f5");
	Результат.Вставить("ГолубойСоСтальнымОттенком", "b0c4de");
	Результат.Вставить("ГрифельноСерый", "708090");
	Результат.Вставить("ГрифельноСиний", "6a5acd");
	Результат.Вставить("Древесный", "deb887");
	Результат.Вставить("ДымчатоБелый", "f5f5f5");
	Результат.Вставить("ЖелтоЗеленый", "9acd32");
	Результат.Вставить("Желтый", "ffff00");
	Результат.Вставить("ЗамшаСветлый", "ffe4b5");
	Результат.Вставить("ЗеленаяЛужайка", "7cfc00");
	Результат.Вставить("ЗеленоватоЖелтый", "7fff00");
	Результат.Вставить("ЗеленоватоЛимонный", "00ff00");
	Результат.Вставить("ЗеленоЖелтый", "adff2f");
	Результат.Вставить("Зеленый", "008000");
	Результат.Вставить("ЗеленыйЛес", "228b22");
	Результат.Вставить("Золотистый", "daa520");
	Результат.Вставить("Золотой", "ffd700");
	Результат.Вставить("Индиго", "4b0082");
	Результат.Вставить("Киноварь", "cd5c5c");
	Результат.Вставить("Кирпичный", "b22222");
	Результат.Вставить("КожаноКоричневый", "8b4513");
	Результат.Вставить("Коралловый", "ff7f50");
	Результат.Вставить("Коричневый", "a52a2a");
	Результат.Вставить("КоролевскиГолубой", "4169e1");
	Результат.Вставить("КрасноФиолетовый", "d02090");
	Результат.Вставить("Красный", "ff0000");
	Результат.Вставить("Кремовый", "fffbf0");
	Результат.Вставить("Лазурный", "f0ffff");
	Результат.Вставить("ЛимонноЗеленый", "32cd32");
	Результат.Вставить("Лимонный", "fffacd");
	Результат.Вставить("Лосось", "fa8072");
	Результат.Вставить("ЛососьСветлый", "ffa07a");
	Результат.Вставить("ЛососьТемный", "e9967a");
	Результат.Вставить("Льняной", "faf0e6");
	Результат.Вставить("Малиновый", "dc143c");
	Результат.Вставить("МятныйКрем", "f5fffa");
	Результат.Вставить("НавахоБелый", "ffdead");
	Результат.Вставить("НасыщенноНебесноГолубой", "00bfff");
	Результат.Вставить("НасыщенноРозовый", "ff1493");
	Результат.Вставить("НебесноГолубой", "87ceeb");
	Результат.Вставить("НейтральноАквамариновый", "66cdaa");
	Результат.Вставить("НейтральноБирюзовый", "48d1cc");
	Результат.Вставить("НейтральноВесеннеЗеленый", "00fa9a");
	Результат.Вставить("НейтральноГрифельноСиний", "7b68ee");
	Результат.Вставить("НейтральноЗеленый", "c0dcc0");
	Результат.Вставить("НейтральноКоричневый", "cd853f");
	Результат.Вставить("НейтральноПурпурный", "9370db");
	Результат.Вставить("НейтральноСерый", "a0a0a4");
	Результат.Вставить("НейтральноСиний", "0000cd");
	Результат.Вставить("НейтральноФиолетовоКрасный", "c71585");
	Результат.Вставить("Оливковый", "808000");
	Результат.Вставить("ОранжевоКрасный", "ff4500");
	Результат.Вставить("Оранжевый", "ffa500");
	Результат.Вставить("Орхидея", "da70d6");
	Результат.Вставить("ОрхидеяНейтральный", "ba55d3");
	Результат.Вставить("ОрхидеяТемный", "9932cc");
	Результат.Вставить("Охра", "a0522d");
	Результат.Вставить("Перламутровый", "fff5ee");
	Результат.Вставить("Персиковый", "ffdab9");
	Результат.Вставить("ПесочноКоричневый", "f4a460");
	Результат.Вставить("ПолночноСиний", "191970");
	Результат.Вставить("ПризрачноБелый", "f8f8ff");
	Результат.Вставить("Пурпурный", "800080");
	Результат.Вставить("Пшеничный", "f5deb3");
	Результат.Вставить("РозовоКоричневый", "bc8f8f");
	Результат.Вставить("Розовый", "ffc0cb");
	Результат.Вставить("Роса", "f0fff0");
	Результат.Вставить("РыжеватоКоричневый", "d2b48c");
	Результат.Вставить("СветлоГрифельноСерый", "778899");
	Результат.Вставить("СветлоГрифельноСиний", "8470ff");
	Результат.Вставить("СветлоЖелтый", "ffffe0");
	Результат.Вставить("СветлоЖелтыйЗолотистый", "fafad2");
	Результат.Вставить("СветлоЗеленый", "90ee90");
	Результат.Вставить("СветлоЗолотистый", "ffec8b");
	Результат.Вставить("СветлоКоралловый", "f08080");
	Результат.Вставить("СветлоКоричневый", "ffe4c4");
	Результат.Вставить("СветлоНебесноГолубой", "87cefa");
	Результат.Вставить("СветлоРозовый", "ffb6c1");
	Результат.Вставить("СветлоСерый", "c0c0c0");
	Результат.Вставить("СеребристоСерый", "dcdcdc");
	Результат.Вставить("Серебряный", "c0c0c0");
	Результат.Вставить("СероСиний", "5f9ea0");
	Результат.Вставить("Серый", "808080");
	Результат.Вставить("СинеСерый", "1e90ff");
	Результат.Вставить("СинеФиолетовый", "8a2be2");
	Результат.Вставить("Синий", "0000ff");
	Результат.Вставить("СинийСоСтальнымОттенком", "4682b4");
	Результат.Вставить("СинийСПороховымОттенком", "b0e0e6");
	Результат.Вставить("Сливовый", "dda0dd");
	Результат.Вставить("СлоноваяКость", "fffff0");
	Результат.Вставить("СтароеКружево", "fdf5e6");
	Результат.Вставить("ТемноБирюзовый", "00ced1");
	Результат.Вставить("ТемноБордовый", "800000");
	Результат.Вставить("ТемноГрифельноСерый", "2f4f4f");
	Результат.Вставить("ТемноГрифельноСиний", "483d8b");
	Результат.Вставить("ТемноЗеленый", "006400");
	Результат.Вставить("ТемноЗолотистый", "b8860b");
	Результат.Вставить("ТемноКрасный", "8b0000");
	Результат.Вставить("ТемноОливковоЗеленый", "556b2f");
	Результат.Вставить("ТемноОранжевый", "ff8c00");
	Результат.Вставить("ТемноСерый", "a9a9a9");
	Результат.Вставить("ТемноСиний", "00008b");
	Результат.Вставить("ТемноФиолетовый", "9400d3");
	Результат.Вставить("ТеплоРозовый", "ff69b4");
	Результат.Вставить("Томатный", "ff6347");
	Результат.Вставить("ТопленоеМолоко", "ffefd5");
	Результат.Вставить("ТусклоОливковый", "6b8e23");
	Результат.Вставить("ТусклоРозовый", "ffe4e1");
	Результат.Вставить("ТусклоСерый", "696969");
	Результат.Вставить("Ультрамарин", "000080");
	Результат.Вставить("Фиолетовый", "ee82ee");
	Результат.Вставить("Фуксин", "ff00ff");
	Результат.Вставить("ФуксинТемный", "8b008b");
	Результат.Вставить("Фуксия", "ff00ff");
	Результат.Вставить("Хаки", "f0e68c");
	Результат.Вставить("ХакиТемный", "bdb76b");
	Результат.Вставить("ЦветМорскойВолны", "2e8b57");
	Результат.Вставить("ЦветМорскойВолныНейтральный", "3cb371");
	Результат.Вставить("ЦветМорскойВолныСветлый", "20b2aa");
	Результат.Вставить("ЦветМорскойВолныТемный", "8fbc8b");
	Результат.Вставить("ЦветокБелый", "fffaf0");
	Результат.Вставить("Циан", "00ffff");
	Результат.Вставить("ЦианАкварельный", "00ffff");
	Результат.Вставить("ЦианНейтральный", "008080");
	Результат.Вставить("ЦианСветлый", "e0ffff");
	Результат.Вставить("ЦианТемный", "008b8b");
	Результат.Вставить("Черный", "000000");
	Результат.Вставить("ШелковыйОттенок", "fff8dc");
	Результат.Вставить("Шоколадный", "d2691e");
	
	Возврат Результат;
	
 КонецФункции //ЦветаВебЗначения 