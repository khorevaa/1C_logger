﻿//
// Кэшируемые методы работы со средой выполнения
// 

// Возвращает код среды выполнения
//
// Параметры: 
// 	СредаВыполнения
//
// Возвращаемое значение:
//   Число
//
Функция Код(СредаВыполнения) Экспорт
	
	Возврат он_СредаВыполненияСервер.Код(СредаВыполнения);
	
КонецФункции //Код 

// Возвращает результат поиска по коду среды исполнения. Обратна к Код()
//
// Параметры: 
// 	Код
//
// Возвращаемое значение:
//   Перечисление.л4с_СредыВыполнения
//
Функция НайтиПоКоду(Код) Экспорт
	
	Возврат он_СредаВыполненияСервер.НайтиПоКоду(Код);
	
КонецФункции //НайтиПоКоду 