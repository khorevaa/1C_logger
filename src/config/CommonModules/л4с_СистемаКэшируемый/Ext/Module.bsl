﻿//
// Кэшируемые системные методы
// 

// Возвращает код текущего устройства
//
// Параметры:
// 	КомпьютерИмя
//
// Возвращаемое значение:
//   Строка
//
Функция УстройствоКод(КомпьютерИмя) Экспорт
	
	Результат	= л4с_МетаданныеАдаптер.УстройствоКод(КомпьютерИмя);
	
	Возврат ?(Результат = Неопределено, КомпьютерИмя, Результат);
	
КонецФункции //УстройствоКод 

// Возвращает имя текущего устройства
//
// Параметры:
// 	КомпьютерИмя
//
// Возвращаемое значение:
//   Строка
//
Функция УстройствоИмя(КомпьютерИмя) Экспорт
	
	Результат	= л4с_МетаданныеАдаптер.УстройствоИмя(КомпьютерИмя);
	
	Возврат ?(Результат = Неопределено, КомпьютерИмя, Результат);
	
КонецФункции //УстройствоИмя 

// Возвращает ссылку на текущее устройство
//
// Параметры:
//
// Возвращаемое значение:
//   Справочник.л4с_Устройства
//
Функция УстройствоТекущее() Экспорт
	
	Возврат л4с_Устройство.Получить(л4с_Система.УстройствоКод(), л4с_Система.УстройствоИмя())
	
КонецФункции //УстройствоТекущее 

//
// Значения РС л4с_Константы
// 

// Возвращает ссылку на текущую ИБ
//
// Параметры:
//
// Возвращаемое значение:
//   Справочник.л4с_ИнформационныеБазы
//
Функция ИнформационнаяБазаТекущая() Экспорт
	
	Возврат л4с_СистемаСервер.ИнформационнаяБазаТекущая();
	
КонецФункции //ИнформационнаяБазаТекущая 

