﻿
// Предопределенный метод
// 
&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	л4с_ДиалогСервер.ПриСозданииНаСервере(ЭтаФорма, Отказ, СтандартнаяОбработка, Ложь);
	Если Отказ Тогда
		Возврат;
	КонецЕсли;
	
	АдаптерМетаданныеВид	= л4с_НастройкиКэшируемый.АдаптерВидМетаданныеСсылка();
	АдаптерМетаданные		= он_АдаптерАктивный.Получить(АдаптерМетаданныеВид);
	
	// В случае отсутствия настроек для текущей ИБ/устройства 
	// создаем настройку по-умолчанию
	л4с_НастройкиСервер.НастройкаУмолчаниеСоздать(л4с_Система.ИнформационнаяБазаТекущая()
	, л4с_Система.УстройствоТекущее());
	
	СписокНастройкиУстройствПараметрыУстановить(Неопределено, Ложь, Ложь);
	
КонецПроцедуры

// Устанавливает параметры списка настроек устройств
//
// Параметры: 
// 	ИнформационнаяБаза
// 	ПоУстройствам
//
&НаСервере
Процедура СписокНастройкиУстройствПараметрыУстановить(ИнформационнаяБаза, ИнформационнаяБазаВключено, ПоУстройствам)
	
	СписокПараметры	= СписокНастройкиУстройств.Параметры;
	
	он_КомпоновкаДанных.ПараметрЗначениеУстановить(СписокПараметры, "ИнформационнаяБаза", ИнформационнаяБаза);
	он_КомпоновкаДанных.ПараметрЗначениеУстановить(СписокПараметры, "ИнформационнаяБазаВключено", ИнформационнаяБазаВключено);
	он_КомпоновкаДанных.ПараметрЗначениеУстановить(СписокПараметры, "ПоУстройствам", ПоУстройствам);
	
КонецПроцедуры //СписокНастройкиУстройствПараметрыУстановить 

// Запись данных на сервере
//
// Параметры: 
//
&НаСервере 
Процедура ЗаписатьНаСервере()
	
	он_АдаптерАктивный.Установить(АдаптерМетаданныеВид, АдаптерМетаданные);
	
КонецПроцедуры //ЗаписатьНаСервере 

// Предопределенный метод
// 
&НаКлиенте
Процедура ПриЗакрытии()
	
	КэшОбновить(Неопределено);
	
КонецПроцедуры

// Обновление кэша 
//
// Параметры: 
//
&НаСервере 
Процедура КэшОбновитьНаСервере()
	
	ОбновитьПовторноИспользуемыеЗначения();
	
КонецПроцедуры //КэшОбновитьНаСервере 

// Обновление кэша
// 
&НаКлиенте
Процедура КэшОбновить(Команда)
	
	#Если МобильноеПриложениеКлиент Тогда
		КэшОбновитьНаСервере();
	#Иначе
		ОбновитьПовторноИспользуемыеЗначения();
	#КонецЕсли
	
КонецПроцедуры

// При активации строки настроек информационных баз
// 
&НаКлиенте
Процедура СписокНастройкиИнформационныхБазПриАктивизацииСтроки(Элемент)
	
	ДанныеТекущие	= Элемент.ТекущиеДанные;
	
	Если ДанныеТекущие <> Неопределено Тогда
		СписокНастройкиУстройствПараметрыУстановить(ДанныеТекущие.ИнформационнаяБаза
		, ДанныеТекущие.Включено
		, ДанныеТекущие.ПоУстройствам);
	КонецЕсли;
	
КонецПроцедуры

// При изменении списка настроек информационных баз
// 
&НаКлиенте
Процедура СписокНастройкиИнформационныхБазПриИзменении(Элемент)
	
	ДанныеТекущие	= Элемент.ТекущиеДанные;
	
	Если ДанныеТекущие <> Неопределено Тогда
		СписокНастройкиУстройствПараметрыУстановить(ДанныеТекущие.ИнформационнаяБаза
		, ДанныеТекущие.Включено
		, ДанныеТекущие.ПоУстройствам);
	КонецЕсли;
	
КонецПроцедуры

// Открытие формы назначения прав пользователям
// 
&НаКлиенте
Процедура ПользователиПрава(Команда)
	
	он_ДиалогКлиент.ФормаОткрыть("Обработка.л4с_ПраваПользователей.Форма.Форма");
	
КонецПроцедуры

// Предопредеелнный метод
// 
&НаКлиенте
Процедура ПередЗакрытием(Отказ, СтандартнаяОбработка)
	
	он_ДиалогКлиент.ФормаПередЗакрытиемМодифицированность(ЭтаФорма
	, "ДанныеЗаписать"
	, Отказ);
	
КонецПроцедуры

// Запись данных
//
// Параметры: 
//
&НаКлиенте 
Процедура ДанныеЗаписать() Экспорт
	
	ЗаписатьНаСервере();
	ЭтаФорма.Модифицированность	= Ложь;
	
КонецПроцедуры //ДанныеЗаписать 

// По команде Записать
// 
&НаКлиенте
Процедура Записать(Команда)
	
	ДанныеЗаписать();
	
КонецПроцедуры

// По команде Записать и закрыть
// 
&НаКлиенте
Процедура ЗаписатьИЗакрыть(Команда)
	
	Записать(Неопределено);
	ЭтаФорма.Закрыть();
	
КонецПроцедуры

// Предопредеелнный метод
//
&НаКлиенте
Процедура ПриОткрытии(Отказ)
	
	он_ДиалогКлиент.ФормаОкноКнопкиПараметрыУстановить(ЭтаФорма);
	
КонецПроцедуры
